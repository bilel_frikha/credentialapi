package com.tunisianscoutassociation.credentialapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CredentialapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CredentialapiApplication.class, args);
	}

}
